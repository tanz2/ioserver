var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
server.listen(process.env.PORT || 3000);

app.set('view engine', 'ejs');
app.set('view options', { layout: false });
app.use(express.methodOverride());
app.use(express.bodyParser());  
app.use(app.router);
app.use('/public', express.static('public'));

app.get('/', function (req, res) {
  res.render('index');
});

io.sockets.on('connection', function(socket) {

	socket.on('bledata', function (userid, username, userbeacon) {
        	var id = userid;
        	var name = username;
        	var beacon = userbeacon;
        	
        	// Verify that correct data has been received
        	var data = 'User ID: '.concat(id,'\n','Name: ',name,'\n','Beacon: ',beacon);
        	io.sockets.emit('updatechat', socket.username, data);
  });

});
